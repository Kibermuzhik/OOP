#include "interactor.h"

interactor::interactor() {
    __item = nullptr;
}

void interactor::start() {
    create();
    menu();
}

interactor::~interactor()
{
    if (__item != nullptr)
    {
        delete __item;
    }
}

void interactor::create() {
    if (__item != nullptr)
    {
        delete __item;
    }
    char type;
    double firstItem, step;
    unsigned int len;
    std::cout << "\nChoose the type of progression [L/E]:\n ";
    std::cin >> type;
    std::cout << "\nEnter progression's first element:\n ";
    std::cin >> firstItem;
    std::cout << "\nEnter progression's step:\n ";
    std::cin >> step;
    std::cout << "\nEnter created numbers line lenght:\n ";
    std::cin >> len;
    try {
        progressionFactory factory;
        __item = factory.buildProgression(toupper(type), firstItem, step, len);
        if (__item != nullptr) 
        {
            std::cout << *__item;
        }
        else 
        {
            std::cout << "\nInvalid progression type. Please, recreate progression.\n\n";
            create();
        }
    }
    catch (std::logic_error err) {
        std::cout << "\n" << err.what() << "Please, recreate the progression.\n\n";
        create();
    }
}


void interactor::menu() {
    int answer = 1;
    unsigned int index;
    while (answer)
    {
        std::cout << "\nWhat would you like to do next? \n1) Find sum of current progression elements\n2) Find an element of current progression by index\n3) Create another progression\n0) Exit program\n ";
        std::cin >> answer;
        switch (answer)
        {
        case 1:
            std::cout << "Up to which element should sum be calculated?\n ";
            std::cin >> index;
            std::cout << "The result is: " << __item->sum(index);
            break;
        case 2:
            std::cout << "\nEnter element's index:\n ";
            std::cin >> index;
            std::cout << "Element number " << index << " is equal to " << __item->calculate(index) << '\n';
            break;
        case 3:
            create();
            break;
        case 0:
            return;
            break;
        }
    }
}




