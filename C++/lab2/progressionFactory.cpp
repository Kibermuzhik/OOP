#include "progressionFactory.h"

progression* progressionFactory::buildProgression(char type, double firstElement, double step, unsigned int length) {
    if (type == 'L') {
        return new linearProgression(length, firstElement, step);
    }
    else if (type == 'E') {
        if (step == 0)
            throw std::logic_error("Exponential progression deominator can't be equal to zero. ");
        return new exponentialProgression(length, firstElement, step);
    }
    else
    {
        return nullptr;
    }

}