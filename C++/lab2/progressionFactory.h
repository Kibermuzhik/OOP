#include "exponentialProgression.h"
#include "linearProgression.h"

class progressionFactory {
public:
    progressionFactory() = default;
    ~progressionFactory() = default;

    progression* buildProgression(char type, double firstElement, double step, unsigned int length);
};