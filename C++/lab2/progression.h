#pragma once
#pragma once
#include <iostream>
#include <stdexcept>

class progression
{
protected:
    double __data;
    unsigned int __len;
    double __step;
public:
    progression(unsigned int len, double firstItem, double step) {
        __len = len;
        __data = firstItem;
        __step = step;
    };
    virtual ~progression() {};
    virtual double sum(unsigned int index) const = 0;
    virtual double calculate(unsigned int index) const = 0;
    friend std::ostream& operator<<(std::ostream& out, const progression& item) {
        out << "Progression step: " << item.__step << '\n';
        out << "Progression length: " << item.__len << '\n';
        for (unsigned int i = 1; i <= item.__len; i++) {
            out << item.calculate(i) << ' ';
        }
        return out;
    }
};