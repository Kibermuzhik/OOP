#include "exponentialProgression.h"

exponentialProgression::exponentialProgression(unsigned int len, double firstItem, double step) :progression(len, firstItem, step) {
}

double exponentialProgression::calculate(unsigned int index) const {
    return __data * pow(__step, index-1);
}

double exponentialProgression::sum(unsigned int index) const{
    double tmpItem = calculate(index);

    if (__step == 1)
    {
        return __data * index;
    }
    return((tmpItem * __step - __data) / (__step - 1));
}




