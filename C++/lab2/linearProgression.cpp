#include "linearProgression.h"


linearProgression::linearProgression(unsigned int len, double firstItem, double step) :progression(len, firstItem, step) {
}

double linearProgression::calculate(unsigned int index) const {
    return __data + (index - 1) * __step;
}

double linearProgression::sum(unsigned int index) const {
    double tmpItem = calculate(index);
    return (((__data + tmpItem) * index)/2);
}



