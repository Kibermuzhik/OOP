#include "progression.h"

class exponentialProgression : public progression
{
public:
    exponentialProgression(unsigned int len, double firstItem, double step);
    ~exponentialProgression() = default;

    double calculate(unsigned int index) const override;
    double sum(unsigned int index) const override;
};  