#include "progression.h"

class linearProgression : public progression
{
public:
    linearProgression(unsigned int len, double firstItem, double step);
    ~linearProgression() = default;

    double sum(unsigned int index) const override;
    double calculate(unsigned int index) const override;
};