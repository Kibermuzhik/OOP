#include "fraction.h"

class interactor{
    private:
    
    fraction __item1, __item2;

    public:

    void menu();
    void cmdOut();
    void createFraction(fraction &createdItem);
    void action(int CMD);
};