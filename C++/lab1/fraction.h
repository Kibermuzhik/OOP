#include <iostream>
#include <stdexcept>
class fraction
{
    private:

    int __numerator;
    int __denominator;

    // Наибольший общий делитель
    int __GCD(int num1, int num2);
    
    // Наименьший общий множитель
    int __LCM(int num1, int num2);

    public:

    fraction();
    fraction(int numerator, int denominator);
    ~fraction(){};

    // Сложение
    fraction add(const fraction &term1, const fraction &term2);
    fraction operator+(const fraction& operand);
    // Вычитание
    fraction subtract(const fraction &minuend, const fraction &subtrahend);
    fraction operator-(const fraction& operand);
    // Умножение
    fraction multiply(const fraction &factor1, const fraction &factor2);
    fraction operator*(const fraction& operand);

    //Деление
    fraction divide(const fraction &divident, const fraction &divider);
    fraction operator/(const fraction& operand);

    // Сравнение
    int compare(const fraction &item2);

    // Упрощение
    fraction simplify();

    // Вывод
    friend std::ostream &operator <<(std::ostream &output, const fraction &item);

};