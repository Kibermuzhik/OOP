#include "fraction.h"

fraction::fraction() : 
__numerator(1), __denominator(1)
{};

fraction::fraction(int numerator, int denominator){
    if (denominator == 0)
        throw std::invalid_argument("Division by zero");
    else 
    {
        __numerator = numerator;
        __denominator = denominator;
    }
}

// Наибольший общий делитель
int fraction::__GCD(int num1, int num2){
    while((num1>0)&&(num2>0)){
        if (num1>num2)
            num1%=num2;
        else 
            num2%=num1;
    }

    return num1 + num2;
}

// Наименьший общий множитель
int fraction::__LCM(int num1, int num2){
    return ((num1*num2)/__GCD(num1, num2));
}

// Сложение
fraction fraction::add(const fraction &term1, const fraction &term2){
    fraction sum;

    int num1 = term1.__numerator, num2 = term2.__numerator,
        den1 = term1.__denominator, den2 = term2.__denominator;

    if (den1 == den2){
        sum.__numerator = num1 + num2;
        sum.__denominator = den1;
    }
    else{
        int lcd = __LCM(den1, den2);
        sum.__numerator = num1*(lcd/den1)+num2*(lcd/den2);
        sum.__denominator = lcd;
    }
    return sum;
}
fraction fraction::operator+(const fraction& operand){
    return add(*this, operand);
}



// Вычитание
fraction fraction::subtract(const fraction &minuend, const fraction &subtrahend){
    fraction diff;

    int num1 = minuend.__numerator, num2 = subtrahend.__numerator,
        den1 = minuend.__denominator, den2 = subtrahend.__denominator;

    if (den1 == den2){
        diff.__numerator = num1 - num2;
        diff.__denominator = den1;
    }
    else{
        int lcd = __LCM(den1, den2);
        diff.__numerator = num1 * (lcd / den1) - num2 * (lcd / den2);
        diff.__denominator = lcd;
    }

    return diff;
}
fraction fraction::operator-(const fraction& operand){
    return subtract(*this, operand);
}


// Умножение
fraction fraction::multiply(const fraction &factor1, const fraction &factor2){
    fraction product;

    product.__numerator = factor1.__numerator * factor2.__numerator;
    product.__denominator = factor1.__denominator * factor2.__denominator;

    return product;
}
fraction fraction::operator*(const fraction& operand){
    return multiply(*this, operand);
}

//Деление
fraction fraction::divide(const fraction &divident, const fraction &divider){
    fraction quotient;

    quotient.__numerator = divident.__numerator * divider.__denominator;
    quotient.__denominator = divident.__denominator * divider.__numerator;

    return quotient;
}
fraction fraction::operator/(const fraction& operand){
    return divide(*this, operand);
}

 // Сравнение
int fraction::compare(const fraction &item2){
    int lcm = __LCM(this->__denominator, item2.__denominator);
    int num1 = this->__numerator*(lcm/this->__denominator), num2 = item2.__numerator*(lcm/item2.__denominator);
    if (num1 == num2)
        return(0);
    else if (num1 > num2)
        return(1);
    else 
        return(2);
}

// Упрощение
fraction fraction::simplify(){
    int gcf = __GCD(this->__numerator, this->__denominator);

    this->__numerator/=gcf;
    this->__denominator/=gcf; 

    return *this;
}

std::ostream &operator<<(std::ostream &output, const fraction &item){
     output<<item.__numerator << '/' << item.__denominator;
    return output;
}

