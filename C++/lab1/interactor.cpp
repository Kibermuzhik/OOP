#include "interactor.h"


void interactor::createFraction(fraction &createdItem){
    int num, den;
            std::cout << "\n Enter numerator:\n";
            std::cin >> num;
            std::cout << "\n Enter denumenator:\n";
            std::cin >> den;
            try{
                createdItem = fraction(num, den);
                std::cout << "\n Successfully created fraction: " << createdItem << '\n';
            }
            catch(std::invalid_argument){
                std::cout << "\n Denumenator can't be equal to zero. Returning to the main menu.\n";
                menu();
            }
}

void interactor::action(int CMD){
    fraction output;
    if((CMD != 6)&&(CMD != 0)){
        std::cout << "\n Create the first fraction:\n";
        createFraction(__item1);
        std::cout << "\n Create the second fraction:\n";
        createFraction(__item2);
    }
    switch(CMD){
        case 1:
            std::cout << " The result is: " << __item1 + __item2 << "\n ";
        break;
        case 2:
            std::cout << " The result is: " << __item1-__item2<< "\n ";
        break;
        case 3:
            std::cout << " The result is: " << __item1*__item2<< "\n ";
        break;
        case 4:
            std::cout << "\n Create the first fraction:\n";
            createFraction(__item1);
            std::cout << "\n Create the second fraction:\n";
            createFraction(__item2);
            std::cout << " The result is: " << __item1/__item2<< "\n ";
        break;
        case 5:
            int res;
            std::cout << "\n Create the first fraction:\n";
            createFraction(__item1);
            std::cout << "\n Create the second fraction:\n";
            createFraction(__item2);
            res = __item1.compare(__item2);
            if (res == 0)
                std::cout << " The fractions are equal\n";
            else if (res == 1)
                std::cout << " The first fraction is greater than the second\n";
            else 
                std::cout << " The first fraction is lower than the second\n ";
        break;
        case 6:
            std::cout<<"\n Create fraction:\n";
            createFraction(__item1);
            std::cout << " Simplified result: " << __item1.simplify() << "\n ";
        break;
        case 0:
            std::cout<< "Are you sure you want to exit?[Y/N]\n";
            char answer;
            std::cin >> answer;
            if(tolower(answer) == 'y')
                exit(0);
            else if (tolower(answer) == 'n')
                menu();
            else 
                action(0);
        break;
    }
    cmdOut();
}



void interactor::cmdOut(){
    int CMD;
    std::cout << "\n 1) Add fractions\n 2) Substract fractions\n 3) Multiply fractions\n 4) Divide fractions\n 5) Compare fractions\n 6) Simplify fraction\n 0) Exit\n";
    std::cin >> CMD;
    action(CMD);
}

void interactor::menu(){
    std::cout << "Choose an action:";
    cmdOut();
}