﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab1
{
    public enum ProgressionType
    {
        Linear = 'L',
        Exponential = 'E'
    }
}
