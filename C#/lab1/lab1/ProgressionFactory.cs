using System;


namespace lab1
{
    class ProgressionFactory
    {
        public IProgression fabricateProgression(ProgressionType type, double fisrtItem, double step, int len)
        {
            if (type == ProgressionType.Linear)
            {
                return new LinearProgression(len, fisrtItem, step);
            }
            else if (type == ProgressionType.Exponential)
            {
                if (step == 0)
                {
                    throw new DivideByZeroException();
                }
                return new ExponentialProgression(len, fisrtItem, step);
            }
            else
            {
                return null;
            }
        } 
    }
}