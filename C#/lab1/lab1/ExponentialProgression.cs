using System;

namespace lab1
{
    class ExponentialProgression : IProgression
    {
        private double _data;
        private double _step;
        private int _len;
        public double Step
        {
            get => _step;
        }
        public int Len
        {
            get => _len;
        }
        public ExponentialProgression(int len, double fisrtItem, double step)
        {
            _len = len;
            _data = fisrtItem;
            _step = step;
        }

        public double sum(int index) 
        {
            double tmpItem = calculate(index);
            if (_step == 1)
            {
                return _data*index;
            }
            return ((tmpItem * _step - _data) / (_step - 1));
        }

        public double calculate(int index)
        {
            return _data * Math.Pow(_step, index-1);
        }
    }
}