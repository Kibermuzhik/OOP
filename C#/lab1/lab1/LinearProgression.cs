using System;

namespace lab1
{
    class LinearProgression : IProgression
    {
        private double _data;
        private double _step;
        private int _len;
        public double Step
        {
            get => _step;
        }
        public int Len
        {
            get => _len;
        }
        public LinearProgression(int len, double fisrtItem, double step)
        {
            _len = len;
            _data = fisrtItem;
            _step = step;
        }

        public double sum(int index) 
        {
            double tmpItem = calculate(index);
            return (((_data+tmpItem) * index)/2);
        }

        public double calculate(int index)
        {
            return _data + (index-1) * _step;
        }
    }
}