using System;

namespace lab1
{
    interface IProgression
    {
        double Step {get;}
        int Len {get;}

        double sum(int index);

        double calculate(int index);
    }
}