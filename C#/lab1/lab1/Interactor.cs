using System;

namespace lab1
{
    
    class Interactor
    {
        private IProgression _item;
        public Interactor()
        {
            _item = null;
        }

        private void writeProgression(IProgression item)
        {
            Console.WriteLine("Progression Step: {0}\n", item.Step);  
            Console.WriteLine("Output length: {0}\n", item.Len);
            for(int i = 1; i <= item.Len; i++)
            {
                Console.Write(item.calculate(i) + " ");
            }
            Console.WriteLine('\n');
        }


        private void menu()
        {
            int answer = 1;
            int index;
            while(answer != 0)
            {
                Console.WriteLine("\nWhat would you like to do next? \n1) Find sum of current progression elements\n2) Find an element of current progression by index\n3) Create another progression\n0) Exit program\n ");
                answer = Convert.ToInt32(Console.ReadLine());
                switch(answer)
                {
                    case 1:
                        Console.WriteLine("Up to which element should sum be calculated?\n ");
                        index = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("The result is: {0}", _item.sum(index));
                    break;
                    case 2:
                        Console.WriteLine("\nEnter element's index:\n ");
                        index = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("The result is: {0}", _item.calculate(index));
                    break;
                    case 3:
                        create();
                    break;
                    case 0:
                        return;
                }
            }
        }

        private void create()
        {
            ProgressionType type;
            double firstItem, step;
            int len;
            bool tryAgain = true;
            while (tryAgain)
            {
            Console.WriteLine("\nChoose the type of progression [L/E]:\n ");
            type = (ProgressionType)Char.ToUpper(Convert.ToChar(Console.ReadLine()));
            Console.WriteLine("\nEnter progression's first element:\n ");
            firstItem = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("\nEnter progression's step:\n ");
            step = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("\nEnter created numbers line lenght:\n ");
            len = Convert.ToInt32(Console.ReadLine());
            try
            {
                ProgressionFactory factory = new ProgressionFactory();
                _item = factory.fabricateProgression(type, firstItem, step, len);
                if (_item != null)
                {
                    writeProgression(_item);
                    tryAgain = false;
                }
                else
                {
                    Console.WriteLine("\nInvalid progression type. Please, recreate progression.\n\n");
                    tryAgain = true;
                }
            }
            catch (DivideByZeroException err)
            {
                Console.WriteLine("\nException caught: {0}\nPlease, recreate progression.\n", err);
                tryAgain = true;
            }
            }

        }

        public void start()
        {
            create();
            menu();
        }
    }
}